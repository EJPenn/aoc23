module Main where
import Data.Char (isDigit)
import Text.Regex.TDFA

extractDigits :: String -> String
extractDigits xs = [x | x <- xs, isDigit x]

wordToNum :: String -> Char
wordToNum "one" = '1'
wordToNum "two" = '2'
wordToNum "three" = '3'
wordToNum "four" = '4'
wordToNum "five" = '5'
wordToNum "six" = '6'
wordToNum "seven" = '7'
wordToNum "eight" = '8'
wordToNum "nine" = '9'
wordToNum x = head x

forwardRegex :: String -> String
forwardRegex xs = xs =~ "one|two|three|four|five|six|seven|eight|nine|[1-9]"

reverseRegex :: String -> String
reverseRegex xs = reverse (reverse xs =~ "[1-9]|enin|thgie|neves|xis|evif|ruof|eerht|owt|eno")

getCalibrationValue :: String -> Int
getCalibrationValue xs = read [head xs, last xs] :: Int

getCalibrationValue2 :: String -> Int
getCalibrationValue2 xs = read (map wordToNum [forwardRegex xs, reverseRegex xs]) :: Int

main = do
    content <- readFile "input"
    let input =  lines content
    print $ sum ( map (getCalibrationValue . extractDigits) input)
    print $ sum ( map getCalibrationValue2 input)